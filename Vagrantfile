Vagrant.require_version ">= 1.8.0"
VAGRANTFILE_API_VERSION = "2"

# Get the environment configuration for Vagrant.
require "yaml"
env = YAML.load_file(File.join(File.dirname(__FILE__), "ansible/host_vars/local/local.yml"))

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	# Set up the box.
	config.vm.box = "bento/ubuntu-16.04"
	config.vm.provider "virtualbox" do |v|
		v.linked_clone = true
	end

	# Set up the network.
	config.vm.network "private_network", ip: env["global_ip"]
	config.vm.hostname = env["global_domain"]

	# Set up the shared folders
	shared_folders = env["global_shared_folders"]
	shared_folders.each do |shared_folder|
		config.vm.synced_folder shared_folder["src"], shared_folder["dest"], shared_folder["options"]
	end

	# Provision the machine
	config.vm.provision "ansible" do |ansible|
		ansible.playbook       = "ansible/playbook.yml"
		ansible.inventory_path = "ansible/inventories/local"
		ansible.limit          = "all"
	end
end