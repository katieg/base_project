# Base Project - Symfony 4.0 with a ReactJS 16 frontend

This is a starting point for a new Symfony 4.0 / ReactJS 16 web application. It includes everything needed to run a local vagrant for development, write unit tests, api tests, and functional tests, have the tests run automatically when branches are pushed to the repository, and to automatically deploy both staging and production when branches are pushed to the repository. It needs to be hosted in a bitbucket repository for the automated builds and testing to work.

# Initial Set Up

1. Clone this project into your project directory.
2. Delete the .git folder.
3. Initialize a new git repository.
4. Ensure that the selenium standalone server with the chrome driver is installed.
5. Copy ansible/group_vars/local_vars/symfony.yml.dist to ansible/group_vars/local_vars/symfony.yml.
6. Go through each of the following files and fill in any missing information, denoted by &lt; &gt; tags:
    - ansible:
        - group_vars:
            - local_vars:
                - symfony.yml
            - prod_vars:
                - certbot_vault.yml
                - deploy.yml
                - deploy_vault.yml
            - staging_vars:
                - certbot_vault.yml
                - deploy.yml
                - deploy_vault.yml
        - host_vars:
            - local:
                - local.yml
            - prod:
                - prod.yml
                - prod_vault.yml
            - staging:
                - staging.yml
                - staging_vault.yml
        - inventories:
            - local
            - prod
            - staging
7. Use ansible-vault to encrypt every file with _vault in the name.
8. Commit the current as your initial commit to your new project. Do not push anything until specified.
9. Create a staging branch from master.
10. Create an integration branch from staging.
11. Create a development branch from integration.
12. Ensure that php7.2 is installed on your system, along with the following packages:
    - php7.2-dom
    - php7.2-zip
    - php7.2-mbstring
13. Ensure that composer is installed on your system.
14. Ensure that node and yarn are installed on your system.
15. Navigate to the symfony folder and run `composer install`. If any issues arise from missing php packages, install the requested package for php7.2 and rerun `composer install`.
16. While still in the symfony folder, run `yarn install`.
17. Navigate to the root of the project, one up from the symfony folder.
18. While at the root of the project, install the ansible galaxy roles using `ansible-galaxy install -r ansible/requirements.yml`.
19. While still at the root of the project, start the vagrant and provision it by running `vagrant up`.
20. You should now be able to access your development environment from the browser using the development url. If you are on a windows system, you will need to add a hosts entry manually to match your development url up with the vagrant ip.
21. Push up the master, staging, integration, and development branches to a new bitbucket repository.
22. Ensure that your staging url is publicly available and pointing to your staging server, and that you can access your staging server using ssh &lt;staging user&gt;@&lt;staging ip&gt;.
23. While still at the root of the project, provision your staging environment using `ansible-playbook -i ansible/inventories/staging ansible/playbook.yml`.
24. You should now be able to access your staging environment from the browser using the staging url.
25. Ensure that your production url is publicly available and pointing to your production server, and that you can access your production server using ssh &lt;prod user&gt;@&lt;prod ip&gt;.
26. While still at the root of the project, provision your production environment using `ansible-playbook -i ansible/inventories/prod ansible/playbook.yml`.
27. You should now be able to access your production environment from the browser using the production url.
28. Create a public/private key pair for Bitbucket's pipelines to access your staging and production servers with. Add the private/public keys to your Bitbucket repository settings for Pipelines, and also add your host keys for your staging and production urls.
29. Create the deployment script from below on your staging server in the home folder of the user you plan to use for deployment. Ensure it is called staging.sh.
29. Create the deployment script from below on your prod server in the home folder of the user you plan to use for deployment. Ensure it is called prod.sh.
31. Enable Bitbucket Pipelines for your repository.

Deployment script:

```
#!/usr/bin/env bash
cd <absolute path to cloned repository>
git pull --ff-only
chown -R webapp:webapp .
sudo -u webapp -H bash -c "
cd <absolute path to cloned repository/symfony>;
touch maintenance.flag
APP_ENV=prod composer install --no-dev --optimize-autoloader;
yarn install;
yarn run encore production;
yarn run encore production --config webpack.config.serverside.js;
APP_ENV=prod /usr/bin/php bin/console cache:warmup;
rm maintenance.flag"
```

# Frontend Development

To have Webpack Encore automatically rebuild and reload your browser window when you are developing the frontend, run `yarn run encore dev --watch` from the symfony folder.

# Tests

To run the various test suites, do the following (from the symfony folder):

- Unit tests:
    - `php vendor/bin/codecept run unit`
- Api tests:
    - `php vendor/bin/codecept run api`
    - `php vendor/bin/codecept run api --env staging`
- Acceptance tests:
    - `php vendor/bin/codecept run acceptance`
    - `php vendor/bin/codecept run acceptance --env staging`
- ReactJS Unit tests:
    - `yarn run jest`