---
- name: enable ssl on nginx config
  replace:
    path: "/etc/nginx/sites-enabled/{{ symfony_domain }}.conf"
    regexp: "#{{ item }}"
    replace: "{{ item }}"
  with_items:
    - "ssl_certificate"
    - "ssl_protocol"
    - "if"
    - "return"
    - "}"
  become: yes
  become_user: "root"

- name: restart nginx
  service:
    name: "nginx"
    state: "restarted"
  become: yes
  become_user: "root"

# Using a command because the copy module is leaving a tmp file behind.
- name: set up environment (dev)
  command: "cp .env.dist .env chdir={{ symfony_root }}"
  when: not symfony_production_mode

- name: set the database information (dev)
  lineinfile:
    path: "{{ symfony_root }}/.env"
    regexp: "^DATABASE_URL="
    line: "DATABASE_URL=mysql://{{ symfony_db_user }}:{{ symfony_db_pass }}@{{ symfony_db_host }}:{{ symfony_db_port }}/{{ symfony_db_name }}"

- name: copy database dump file
  copy:
    src: "{{ playbook_dir }}/files/symfony/db.sql.gz"
    dest: /tmp

- name: restore database
  mysql_db:
    name: "{{ symfony_db_name }}"
    state: "import"
    target: /tmp/db.sql.gz
    login_user: root
    login_password: "{{ symfony_root_db_pass }}"

# Using a command because the copy module is leaving a tmp file behind.
- name: set up testing configs
  command: "cp {{ item.src }} {{ item.dest }} chdir={{ symfony_root }}/tests"
  with_items:
    - src: "api.suite.yml.dist"
      dest: "api.suite.yml"
    - src: "acceptance.suite.yml.dist"
      dest: "acceptance.suite.yml"
  when: not symfony_production_mode

- name: set the api testing information
  lineinfile:
    path: "{{ symfony_root }}/tests/api.suite.yml"
    regexp: "^            url: \".+\" # dev"
    line: "            url: \"http://{{ symfony_domain }}/api/v1\" # dev"
  with_items:
    - regexp: "^            url: \".+\" # dev"
      line: "            url: \"http://{{ symfony_domain }}/api/v1\" # dev"
    - regexp: "^                    url: \".+\" # staging"
      line: "                    url: \"https://{{ symfony_staging_domain }}/api/v1\" # dev"
  when: not symfony_production_mode

- name: set the acceptance testing information
  lineinfile:
    path: "{{ symfony_root }}/tests/acceptance.suite.yml"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    backrefs: true
  with_items:
    - regexp: "^            url: \"http://.+/\" # dev$"
      line: "            url: \"http://{{ symfony_domain }}/\" # dev"
    - regexp: "^            url: \"http://.+/\" # staging$"
      line: "            url: \"http://{{ symfony_staging_domain }}/\" # staging"
    - regexp: "^            0: java.exe -Dwebdriver.chrome.driver=\".+\" -jar \".+\"$"
      line: "            0: {{ symfony_selenium_command }}"
  when: not symfony_production_mode

- name: run composer installer
  composer:
    command: install
    working_dir: "{{ symfony_root }}"
    no_dev: true
    optimize_autoloader: true
  when: symfony_production_mode
  environment:
    APP_ENV: "{{ symfony_env }}"

- name: run yarn installer
  command: "yarn install chdir={{ symfony_root }}"
  when: symfony_production_mode

- name: build frontend
  command: "yarn run encore production chdir={{ symfony_root }}"
  when: symfony_production_mode

- name: build server-side code
  command: "yarn run encore production --config webpack.config.serverside.js chdir={{ symfony_root }}"
  when: symfony_production_mode

- name: warm the cache
  command: "/usr/bin/php bin/console cache:warmup chdir={{ symfony_root }}"
  environment:
    APP_ENV: "{{ symfony_env }}"