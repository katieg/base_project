<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class Index extends Controller
{
    /**
     * @Route("/admin")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function example(Request $request)
    {
        $initialState = ['baseUrl' => '/admin'];

        if ($request->get('preloadCounter')) {
            $initialState['counter'] = 10;
        }

        return $this->render('admin/index.html.twig', [
            'initialState' => $initialState
        ]);
    }
}