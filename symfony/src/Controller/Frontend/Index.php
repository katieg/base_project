<?php

namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class Index extends Controller
{
    /**
     * @Route("/")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function example(Request $request)
    {
        $initialState = ['counter' => null, 'baseUrl' => '/'];

        if ($request->get('preloadCounter')) {
            $initialState['counter'] = 5;
        }

        return $this->render('frontend/index.html.twig', [
            'initialState' => $initialState
        ]);
    }
}