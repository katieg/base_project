<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class Counter extends Controller
{
    /**
     * @Route("/api/counter")
     * @Method({"GET"})
     */
    public function example()
    {
        return $this->json(1);
    }
}