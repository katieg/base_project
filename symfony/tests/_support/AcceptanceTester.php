<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

	/**
	 * @When /^I go to (.+)$/
	 */
	public function iGoTo($url)
	{
		$this->amOnPage($url);
	}

	/**
	 * @Then /^I see a page with "(.+)"\.$/
	 */
	public function iSeeAPageWith($text)
	{
		sleep(2);
		$this->see($text, '.counter');
	}
}
