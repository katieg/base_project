<?php
$I = new ApiTester($scenario);
$I->wantTo('get the counter and see the result');
$I->sendGET('/counter');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsJson();
$I->seeResponseContains('1');
