var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where compiled assets will be stored
  .setOutputPath('server/build/')
  // the public path used by the web server to access the previous directory
  .setPublicPath('/')
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  // uncomment to create hashed filenames (e.g. app.abc123.css)
  .enableVersioning(Encore.isProduction())

  // uncomment to define the assets of the project
  .addEntry('app', ['babel-polyfill', './assets/server-main.js'])
  // .addStyleEntry('css/app', './assets/css/app.scss')

  // uncomment if you use Sass/SCSS files
  //.enableSassLoader()

  // uncomment for legacy applications that require $/jQuery as a global variable
  // .autoProvidejQuery()

  // Enable react
  .enableReactPreset()

  // Ignore scss files
  .addLoader({ test: /\.scss$/, loader: 'ignore-loader' })

  // Include eslinting in the airbnb style.
  .addLoader({
    test: /\.(js|jsx)$/,
    loader: 'eslint-loader',
    exclude: [/node_modules/],
    enforce: 'pre',
    options: {
      configFile: './.eslintrc',
      emitWarning: true
    }
  })
;

var config = Encore.getWebpackConfig();

config.watchOptions = { poll: true, ignored: /node_modules/ };

module.exports = config;
