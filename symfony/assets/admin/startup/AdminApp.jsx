import React from 'react';
import { BrowserRouter, StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import ReactOnRails from 'react-on-rails';

import Root from '../containers/Root';
import '../css/style.scss';

const mainNode = (_initialProps, context) => {
  const store = ReactOnRails.getStore('adminStore');
  const { location, base, serverSide } = context;

  if (serverSide) {
    return (
      <Provider store={store}>
        <StaticRouter basename={base} location={location} context={{}}>
          <Root />
        </StaticRouter>
      </Provider>
    );
  }

  return (
    <Provider store={store}>
      <BrowserRouter basename={base}>
        <Root />
      </BrowserRouter>
    </Provider>
  );
};

export default mainNode;
