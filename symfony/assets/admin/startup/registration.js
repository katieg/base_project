import ReactOnRails from 'react-on-rails';

import AdminApp from './AdminApp';
import configureStore from '../store/AdminStore';

const adminStore = configureStore;

ReactOnRails.registerStore({ adminStore });
ReactOnRails.register({ AdminApp });
