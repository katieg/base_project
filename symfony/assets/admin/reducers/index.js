import adminReducer, { initialState as adminState } from './adminReducer';

export default adminReducer;

export const initialStates = {
  adminState,
};
