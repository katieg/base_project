import Constants from '../constants/adminConstants';

export const initialState = {
  counter: null,
  baseUrl: '/',
  location: '/',
  fetching: false,
};

export default function adminReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.COUNTER_FETCHING:
      return { ...state, fetching: true };
    case Constants.COUNTER_RECEIVED:
      return { ...state, fetching: false, counter: action.counter };
    default:
      return state;
  }
}
