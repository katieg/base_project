const Constants = {
  COUNTER_FETCHING: 'COUNTER_FETCHING',
  COUNTER_RECEIVED: 'COUNTER_RECEIVED',
};

export default Constants;
