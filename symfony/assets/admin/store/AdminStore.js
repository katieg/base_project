import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducers, { initialStates } from '../reducers';

export default function configureStore(props, context) {
  const { counter } = props;
  const { base, location } = context;
  const { adminState } = initialStates;

  const initialState = {
    ...adminState,
    counter,
    baseUrl: base,
    location,
  };

  return createStore(reducers, initialState, compose(applyMiddleware(thunkMiddleware)));
}
