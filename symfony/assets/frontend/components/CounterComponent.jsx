import React from 'react';
import PropTypes from 'prop-types';

const CounterComponent = props => (
  <div className="counter">
    Hi! My counter value is: {props.counter}
  </div>
);

CounterComponent.propTypes = {
  counter: PropTypes.number.isRequired,
};

export default CounterComponent;
