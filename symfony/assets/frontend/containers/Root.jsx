import { Route } from 'react-router-dom';
import React from 'react';

import Counter from '../containers/Counter';

const Root = () => (
  <div>
    <Route path="/" exact component={Counter} />
  </div>
);

export default Root;
