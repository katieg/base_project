import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Actions from '../actions/appActions';
import CounterComponent from '../components/CounterComponent';

class Counter extends React.Component {
  componentDidMount() {
    if (!this.props.counter) {
      this.props.dispatch(Actions.fetchCounter(this.props.baseUrl));
    }
  }

  render() {
    if (this.props.fetching || !this.props.counter) {
      return (
        <div>
          Loading...
        </div>
      );
    }

    return (
      <div>
        <CounterComponent counter={this.props.counter} routePrefix="" />
      </div>
    );
  }
}

Counter.propTypes = {
  counter: PropTypes.number,
  dispatch: PropTypes.func.isRequired,
  baseUrl: PropTypes.string.isRequired,
  fetching: PropTypes.bool.isRequired,
};

Counter.defaultProps = {
  counter: null,
};

const mapStateToProps = state => (
  {
    counter: state.counter,
    fetching: state.fetching,
    baseUrl: state.baseUrl,
  }
);

export default connect(mapStateToProps)(Counter);
