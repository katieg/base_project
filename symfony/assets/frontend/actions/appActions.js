import Constants from '../constants/appConstants';

const Actions = {
  fetchCounter: baseUrl => (
    (dispatch) => {
      dispatch({ type: Constants.COUNTER_FETCHING });

      fetch(`${baseUrl}/api/counter`).then(response => (
        response.json()
      )).then((data) => {
        dispatch({
          type: Constants.COUNTER_RECEIVED,
          counter: data,
        });
      });
    }
  ),
};

export default Actions;
