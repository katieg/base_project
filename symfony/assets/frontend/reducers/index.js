import appReducer, { initialState as appState } from './appReducer';

export default appReducer;

export const initialStates = {
  appState,
};
