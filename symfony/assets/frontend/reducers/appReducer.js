import Constants from '../constants/appConstants';

export const initialState = {
  counter: null,
  baseUrl: '/',
  location: '/',
  fetching: false,
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.COUNTER_FETCHING:
      return { ...state, fetching: true };
    case Constants.COUNTER_RECEIVED:
      return { ...state, fetching: false, counter: action.counter };
    default:
      return state;
  }
}
