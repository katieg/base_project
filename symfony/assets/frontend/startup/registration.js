import ReactOnRails from 'react-on-rails';
import App from './App';
import configureStore from '../store/AppStore';

const appStore = configureStore;

ReactOnRails.registerStore({ appStore });
ReactOnRails.register({ App });
