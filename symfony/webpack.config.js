var Encore = require('@symfony/webpack-encore');
var LiveReloadPlugin = require('webpack-livereload-plugin');

Encore
// Set the directory to build the assets to.
  .setOutputPath('public/build/')

  // Set the public path used by the web server to access the build directory from the document root.
  .setPublicPath('/build')

  .cleanupOutputBeforeBuild()

  // Generate source maps for development environments
  .enableSourceMaps(!Encore.isProduction())

  // Set up versioned output files for production environments
  .enableVersioning(Encore.isProduction())

  // Generate the frontend assets.
  .addEntry('frontend', ['whatwg-fetch', 'babel-polyfill', './assets/frontend/main.js'])

  // Generate the admin assets.
  .addEntry('admin', ['whatwg-fetch', 'babel-polyfill', './assets/admin/main.js'])

  // Handle any Sass/Scss files required in the javascript and bundle them into css files.
  .enableSassLoader()

  // Enable React (jsx) processing
  .enableReactPreset()

  // Include the LiveReload plugin to trigger the browser to reload when files are changed.
  .addPlugin(new LiveReloadPlugin())

  // Include eslinting in the airbnb style.
  .addLoader({
    test: /\.(js|jsx)$/,
    loader: 'eslint-loader',
    exclude: [/node_modules/],
    enforce: 'pre',
    options: {
      configFile: './.eslintrc',
      emitWarning: true
    }
  })
;

// Generate the config.
var config = Encore.getWebpackConfig();

// Update the watch options to:
// 1. use polling in case the --watch is run on a Windows system (or WSL).
// 2. don't care about changes in the node_modules folder.
config.watchOptions = { poll: true, ignored: /node_modules/ };

// Export the config.
module.exports = config;
