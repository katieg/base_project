import React from 'react';
import {shallow} from 'enzyme';
import CounterComponent from '../assets/frontend/components/CounterComponent.jsx';

test('Counter component has Hi! My counter value is: 1 text', () => {
    const index = shallow(<CounterComponent counter={1} />);

    expect(index.find('div').text()).toEqual('Hi! My counter value is: 1');
});